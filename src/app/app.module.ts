import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LocalcontextComponent } from './localcontext/localcontext.component';
import { LocalcontextSearchboundComponent } from './localcontext-searchbound/localcontext-searchbound.component';
import { MapPlacesComponent } from './map-places/map-places.component';

@NgModule({
  declarations: [
    AppComponent,
    LocalcontextComponent,
    LocalcontextSearchboundComponent,
    MapPlacesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
