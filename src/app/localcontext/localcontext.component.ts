import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
// import { } from 'googlemaps';

declare var google;


@Component({
  selector: 'app-localcontext',
  templateUrl: './localcontext.component.html',
  styleUrls: ['./localcontext.component.css']
})
export class LocalcontextComponent implements OnInit {

  container: HTMLBaseElement;
  @ViewChild('mapRef', { static: true }) mapElement: ElementRef;

  localContextMapView: any

  constructor() { }

  // mapkey = 'YOUR_API_KEY';
  mapkey = 'AIzaSyDVU0b1M6aNsetB-1cOBtMdv-9I5-1IN7I';
  url = 'https://maps.googleapis.com/maps/api/js?key=' + this.mapkey + '&callback=initMap&libraries=localContext&v=beta';

  ngOnInit() {
    // this.renderMap()

    this.loadScript()
  }


  loadScript() {
    window['initMap'] = () => {
      this.initmap();
    }
    if (!window.document.getElementById('script')) {
      let node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      window.document.body.appendChild(node)
    } else {
      this.initmap()
    }
  }

  initmap = () => {
    let map;
    const localContextMapView = new google.maps.localContext.LocalContextMapView({
      element: document.getElementById("map"),
      placeTypePreferences: ["restaurant", "tourist_attraction"],
      maxPlaceCount: 12,

    });
    map = localContextMapView.map;
    map.setOptions({
      center: { lat: 51.507307, lng: -0.08114 },
      zoom: 14,
    });
  }


  renderMap() {

    window['initMap'] = () => {
      this.loadMap();
    }
    if (!window.document.getElementById('script')) {
      var s = window.document.createElement("script");
      // s.id = "google-map-script";
      s.type = "text/javascript";
      s.src = this.url;

      window.document.body.appendChild(s);
    } else {
      this.loadMap();
    }
  }



  loadMap = () => {
    var map = new window['google'].maps.Map(this.mapElement.nativeElement, {
      center: { lat: 51.507307, lng: -0.08114 },
      zoom: 8
    });


    var marker = new window['google'].maps.Marker({
      position: { lat: 51.507307, lng: -0.08114 },
      map: map,
      title: 'Hello World!',
      draggable: true,
      animation: window['google'].maps.Animation.DROP,

    });

    var contentString = '<div id="content">' +
      '<div>' +
      'example point'
    '</div>' +
      '</div>';

    var infowindow = new window['google'].maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', function () {
      infowindow.open(map, marker);
    });

  }




}
