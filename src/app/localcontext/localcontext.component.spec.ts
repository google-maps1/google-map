import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalcontextComponent } from './localcontext.component';

describe('LocalcontextComponent', () => {
  let component: LocalcontextComponent;
  let fixture: ComponentFixture<LocalcontextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalcontextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalcontextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
