import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalcontextSearchboundComponent } from './localcontext-searchbound/localcontext-searchbound.component';
import { LocalcontextComponent } from './localcontext/localcontext.component';
import { MapPlacesComponent } from './map-places/map-places.component';


const routes: Routes = [
  {
    path: 'local-context',
    component: LocalcontextComponent
  },
  {
    path: 'local-context-sb',
    component: LocalcontextSearchboundComponent
  },
  {
    path:'places',
    component:MapPlacesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
