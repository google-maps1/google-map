import { Component, OnInit } from '@angular/core';
declare var google;

@Component({
  selector: 'app-map-places',
  templateUrl: './map-places.component.html',
  styleUrls: ['./map-places.component.css']
})
export class MapPlacesComponent implements OnInit {
 // mapkey = 'YOUR_API_KEY';
 mapkey = 'AIzaSyDVU0b1M6aNsetB-1cOBtMdv-9I5-1IN7I';
 url = 'https://maps.googleapis.com/maps/api/js?key=' + this.mapkey + '&callback=initMap&libraries=places';

  constructor() { }

  ngOnInit() {
    this.loadScript()
  }
  loadScript() {
    window['initMap'] = () => {
      this.initMap();
    };
    if (!window.document.getElementById('script')) {
      let node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      window.document.body.appendChild(node);
    } else {
      this.initMap();
    }
  }

  initMap = () => {
    let map;
    let service;
    let infowindow;

    const sydney = new google.maps.LatLng(-33.867, 151.195);
    infowindow = new google.maps.InfoWindow();
    map = new google.maps.Map(document.getElementById("map"), {
      center: sydney,
      zoom: 15,
    });
    const request = {
      query: "Museum of Contemporary Art Australia",
      fields: ["name", "geometry"],
    };
    service = new google.maps.places.PlacesService(map);
    service.findPlaceFromQuery(request, (results, status) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (let i = 0; i < results.length; i++) {
          this.createMarker(results[i],map);
        }
        map.setCenter(results[0].geometry.location);
      }
    });

  };

  createMarker(place,map) {
    let infowindow;
    const marker = new google.maps.Marker({
      map,
      position: place.geometry.location,
      icon:"assets/poi.jpg"
    });
    google.maps.event.addListener(marker, "click", () => {
      infowindow.setContent(place.name);
      infowindow.open(map);
    });
  }


}
