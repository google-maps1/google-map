import { Component, OnInit } from '@angular/core';

declare var google;

@Component({
  selector: 'app-localcontext-searchbound',
  templateUrl: './localcontext-searchbound.component.html',
  styleUrls: ['./localcontext-searchbound.component.css']
})
export class LocalcontextSearchboundComponent implements OnInit {

  mapkey = 'AIzaSyDVU0b1M6aNsetB-1cOBtMdv-9I5-1IN7I';
  url = 'https://maps.googleapis.com/maps/api/js?key=' + this.mapkey + '&callback=initMap&libraries=localContext&v=beta';

  constructor() { }

  ngOnInit() {
    this.loadScript();
  }

  loadScript() {
    window['initMap'] = () => {
      this.initMap();
    }
    if (!window.document.getElementById('script')) {
      let node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      window.document.body.appendChild(node)
    } else {
      this.initMap()
    }
  }

  initMap() {
    let map;
    const center = { lat: 37.4219998, lng: -122.0840572 };
    const bigBounds = {
      north: 37.432,
      south: 37.412,
      west: -122.094,
      east: -122.074,
    };
    const localContextMapView = new google.maps.localContext.LocalContextMapView(
      {
        element: document.getElementById("map"),
        placeTypePreferences: ["restaurant"],
        maxPlaceCount: 12,
        locationRestriction: bigBounds,
        directionsOptions: { origin: center },
      }
    );
    map = localContextMapView.map;
    new google.maps.Marker({ position: center, map: map });
    map.setOptions({
      center: center,
      zoom: 16,
    });
  }


}
