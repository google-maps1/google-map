import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalcontextSearchboundComponent } from './localcontext-searchbound.component';

describe('LocalcontextSearchboundComponent', () => {
  let component: LocalcontextSearchboundComponent;
  let fixture: ComponentFixture<LocalcontextSearchboundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalcontextSearchboundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalcontextSearchboundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
